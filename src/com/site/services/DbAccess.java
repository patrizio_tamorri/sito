package com.site.services;

import java.util.ArrayList;
import java.util.HashMap;

import com.crystal.engine.Reus.sqlSupport.SQL;
import com.crystal.engine.Reus.sqlSupport.SqlResultHandler;

public interface DbAccess {

	public ArrayList<HashMap<String, Object>> getMainPageState() throws Exception;
	public ArrayList<HashMap<String, Object>> getRederPageState() throws Exception;
	public ArrayList<HashMap<String, Object>> getBrokenPromisesPageState() throws Exception;
}
