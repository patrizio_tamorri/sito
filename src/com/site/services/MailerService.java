package com.site.services;

public interface MailerService {

	
	public void sendMail(String name,String surname,String email,String body) throws Exception;
	
}
