package com.site.services.impl;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import com.crystal.engine.Reus.sqlSupport.SQL;
import com.crystal.engine.Reus.sqlSupport.SqlResultHandler;
import com.site.services.DbAccess;

import oracle.sql.BLOB;

@Service
public class DbAccessImpl implements DbAccess {

	public ArrayList<HashMap<String, Object>> getMainPageState() throws Exception {
		SQL sql = SQL.initSQLStandardConnection("MAIN SITE");
		sql.openConnection();
		SqlResultHandler rs = sql.executeSelectStatementOnQuery("SELECT TITLE, BODY, LINK FROM site_db.notizie_main_page order by INI_DATE desc", null);
		ArrayList<HashMap<String, Object>> res = new ArrayList<HashMap<String, Object>>();
		while (rs.next()) {
			HashMap<String, Object> x = new HashMap<>();
			for (String nn : rs.getColumnNames())
				{
				if(rs.getColumnTypes().get(nn) != SQL.DB_DATATYPE_BLOB)
					x.put(nn, rs.getObject(nn));
				else
					x.put(nn, IOUtils.toByteArray(((BLOB)rs.getObject(nn)).binaryStreamValue()));
				}
			res.add(x);
		}
		sql.closeConnection();
		return res;
	}

	
	public ArrayList<HashMap<String, Object>> getRederPageState() throws Exception {
		SQL sql = SQL.initSQLStandardConnection("MAIN SITE");
		sql.openConnection();
		SqlResultHandler rs = sql.executeSelectStatementOnQuery("SELECT TITLE, BODY, LINK FROM site_db.notizie_reder_page order by INI_DATE desc", null);
		ArrayList<HashMap<String, Object>> res = new ArrayList<HashMap<String, Object>>();
		while (rs.next()) {
			HashMap<String, Object> x = new HashMap<>();
			for (String nn : rs.getColumnNames())
				x.put(nn, rs.getObject(nn));
			res.add(x);
		}
		sql.closeConnection();
		return res;
	}
	
	
	public ArrayList<HashMap<String, Object>> getBrokenPromisesPageState() throws Exception {
		SQL sql = SQL.initSQLStandardConnection("MAIN SITE");
		sql.openConnection();
		SqlResultHandler rs = sql.executeSelectStatementOnQuery("SELECT TITLE, BODY, LINK FROM notizie_broken_promises_page order by INI_DATE desc", null);
		ArrayList<HashMap<String, Object>> res = new ArrayList<HashMap<String, Object>>();
		while (rs.next()) {
			HashMap<String, Object> x = new HashMap<>();
			for (String nn : rs.getColumnNames())
				x.put(nn, rs.getObject(nn));
			res.add(x);
		}
		sql.closeConnection();
		return res;
	}

	
	
	
}
