package com.site.controllers.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.crystal.engine.Radius.spring.annotations.MainPageTemplate;
import com.crystal.engine.Radius.spring.models.JsonResponseWrapper;
import com.crystal.engine.Reus.Framework;
import com.site.services.DbAccess;
import com.site.services.MailerService;

@Controller
public class IndexPageController {

	@Autowired
	MailerService mailServ;

	@Autowired
	DbAccess db;

	@RequestMapping(value = { "/", "" })
	@MainPageTemplate
	public ModelAndView home(HttpSession session) {
		if (session.getAttribute("THEME") == null) {
			session.setAttribute("THEME", 1);
			session.setAttribute("DATEPICK_THEME", 1);
			session.setAttribute("SLIDER_THEME", 1);
		}

		return new ModelAndView("index");
	}

	@RequestMapping(value = "/Reder_Engine")
	@MainPageTemplate
	public ModelAndView Reder_entryPoint(HttpSession session) {
		if (session.getAttribute("THEME") == null) {
			session.setAttribute("THEME", 1);
			session.setAttribute("DATEPICK_THEME", 1);
			session.setAttribute("SLIDER_THEME", 1);
		}

		return new ModelAndView("Reder_Engine");
	}

	@RequestMapping(value = "/Broken_Promises")
	@MainPageTemplate
	public ModelAndView Broken_Promises_entryPoint(HttpSession session) {
		if (session.getAttribute("THEME") == null) {
			session.setAttribute("THEME", 1);
			session.setAttribute("DATEPICK_THEME", 1);
			session.setAttribute("SLIDER_THEME", 1);
		}

		return new ModelAndView("Broken_Promises_Game");
	}

	@RequestMapping(value = "/Contacts")
	public ModelAndView Contacts(HttpSession session) {
		if (session.getAttribute("THEME") == null) {
			session.setAttribute("THEME", 1);
			session.setAttribute("DATEPICK_THEME", 1);
			session.setAttribute("SLIDER_THEME", 1);
		}

		return new ModelAndView("Contacts");
	}

	@RequestMapping(value = "/sendContactMail")
	public @ResponseBody JsonResponseWrapper sendContactMail(@RequestParam Map<String, String> params) {
		JsonResponseWrapper jr = new JsonResponseWrapper();

		try {
			String name = params.get("name");
			String surname = params.get("surname");
			String email = params.get("mail");
			String body = params.get("body");

			if (name == null || name.trim().equalsIgnoreCase("") || surname == null
					|| surname.trim().equalsIgnoreCase("") || email == null || email.trim().equalsIgnoreCase("")
					|| body == null || body.trim().equalsIgnoreCase(""))
				throw new Exception("Error in parameters");
			mailServ.sendMail(name, surname, email, body);

			jr.getSuccessMessages().put("success", "Message Sent Succefully!");
		} catch (Exception e) {
			Framework.log_error("Cannot Send Mail", e);
			jr.getErrorsMessages().put("error", "Message Was Not Sent! Internal Error!");
		}

		return jr;
	}

	@RequestMapping(value = "/getMainNews")
	public @ResponseBody JsonResponseWrapper getMainNews(@RequestParam Map<String, String> params) throws Exception {
		JsonResponseWrapper jr = new JsonResponseWrapper();
		try 
		{
			ArrayList<HashMap<String, Object>> datas = db.getMainPageState();
			jr.setUserDatas(datas);			

		} catch (Exception e) {
			Framework.log_error("Cannot get main news", e);
			jr.getErrorsMessages().put("error", "Cannot get main News. Error!!");
		}

		return jr;
	}
	
	
	@RequestMapping(value = "/getRederNews")
	public @ResponseBody JsonResponseWrapper getRederNews(@RequestParam Map<String, String> params) throws Exception {
		JsonResponseWrapper jr = new JsonResponseWrapper();
		try 
		{
			ArrayList<HashMap<String, Object>> datas = db.getRederPageState();
			jr.setUserDatas(datas);			

		} catch (Exception e) {
			Framework.log_error("Cannot get reder news", e);
			jr.getErrorsMessages().put("error", "Cannot get main News. Error!!");
		}

		return jr;
	}
	
	@RequestMapping(value = "/getBrokenPromisesNews")
	public @ResponseBody JsonResponseWrapper getBrokenPromisesNews(@RequestParam Map<String, String> params) throws Exception {
		JsonResponseWrapper jr = new JsonResponseWrapper();
		try 
		{
			ArrayList<HashMap<String, Object>> datas = db.getBrokenPromisesPageState();
			jr.setUserDatas(datas);			

		} catch (Exception e) {
			Framework.log_error("Cannot get broken promises news", e);
			jr.getErrorsMessages().put("error", "Cannot get main News. Error!!");
		}

		return jr;
	}
	

}
