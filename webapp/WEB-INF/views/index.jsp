
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="google-site-verification"
	content="iumrzZBmfatgIbTXdeQY2Qnei7LwqlfAjvcycP334_0" />
<title>Tamorri Interactive Studios</title>
<link rel="icon" media="image/png" href="/resources/imgs/favicon.ico" />
<r_radius_imports
	general_theme_number="<%=(Integer)session.getAttribute("THEME")%>"
	datepicker_theme_number="<%=(Integer)session.getAttribute("DATEPICK_THEME")%>"
	slider_theme_number="<%=(Integer)session.getAttribute("SLIDER_THEME")%>" />
</head>
<r_seo_specs
	description="Tamorri Interactive Studios is a videogame producer society based in Rome"
	keywords="Tamorri,Interactive,studios,directx,12" language="en"
	robots="all" />
<body>

<script type="text/javascript">
$.unblockUI();
setPageBlockCallback(function(){});
$.unblockUI();
</script>


	<%@include file="support/Template.jsp"%>

	<div id="navArea" style='min-height: 90%;'>

		<div class="jumbotron">
			<div class="container">
				<h1 class="display-3">Tamorri Interactive Studios</h1>
				<div class="row">
					<div class="col-sm-4" >
						<video autoplay loop muted width="350" height="275" style='border: 6px double rgba(132, 20, 20, 0.55);'>
							<source src="/resources/videos/Logo_TIS.mp4" type="video/mp4" />
						</video>

					</div>
					<div class="col-sm-7">
						<p style='padding-top: 64px; padding-left: 10px;'>Tamorri Interactive Studios(TIS) is born in Rome in 2011,
							during Res Bellicae Mod project. It's currently producing a new
							videogame, "Broken Promises", based on home-made engine, "Reder".
							It's founder, Patrizio Tamorri, is currently working on it.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container" style='padding-bottom: 30px'>
			<div class="row" id="news"></div>
		</div>


		<script>

$(function(){

	callController("getMainNews",{},"POST",function(datas){
		
		for(var i in datas)
			$("#news").append(" <div class=\"col-md-6 grad\"   ><h2 id='mainnews'>"+datas[i]["TITLE"]+"</h2><p>"+ datas[i]["BODY"] +"</p>" +
					(datas[i]["LINK"] != null ? "<p><a class=\"btn btn-secondary\" href=\"#\" onclick='goToPage(\"#navArea\",\""+datas[i]["LINK"]+"\",true);return false;' role=\"button\">Find more &raquo;</a></p>" : "") +
		       "</div>");
		return true;		
	});
	
	
});

</script>





	</div>

	<footer style='bottom: 2px; top: auto; position:relative ; width: 100%'>
		<hr />
		<p>&copy; All rights reserved</p>
	</footer>

</body>
</html>
