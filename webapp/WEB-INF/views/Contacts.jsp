<%@page import="com.crystal.engine.Radius.spring.RadiusPageHelpers"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
 <title>Tamorri Interactive Studios</title>
<link rel="icon" media="image/png" href="/resources/imgs/favicon.ico"/>

<r_seo_specs 
description="Tamorri Interactive studios is a videogame producer based in Rome" 
keywords="" 
language="en"
robots="noindex, nofollow"
/>
 
<r_radius_imports
	general_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	datepicker_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	slider_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	forceSecurity />
</head>
<body>


<script type="text/javascript">
setPageBlockCallback(function(){$.blockUI({ message: null})});
</script>



<%@include file="support/Template.jsp" %>

	<div class="container-fluid" style='padding-top: 100px'>

		<div class="row">
		
			<div class="col-md-6 col-md-offset-3">
				<h1>Contact the team:</h1>
			</div>
			 <div class="col-md-3"></div>
			</div>
			<div class="row">
			<div class="form-group">
			  <label class="col-md-2 col-md-offset-2 control-label text-right">First Name</label>  
			  <div class="col-md-4 inputGroupContainer">
			  <div class="input-group">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  <input  id="first_name" placeholder="First Name" class="form-control"  type="text"/>
			    </div>
			  </div>
			   <div class="col-md-4"></div>
			</div>
			</div>
			<div class="row">
			<div class="form-group">
			  <label class="col-md-2 col-md-offset-2  control-label text-right" >Last Name</label> 
			    <div class="col-md-4 inputGroupContainer">
			    <div class="input-group">
			  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			  <input id="last_name" placeholder="Last Name" class="form-control"  type="text"/>
			    </div>
			  </div>
			  <div class="col-md-4"></div>
			</div>
			
</div>
			<div class="row">
	       <div class="form-group">
			  <label class="col-md-2 col-md-offset-2  control-label text-right">E-Mail</label>  
			    <div class="col-md-4 inputGroupContainer">
			    <div class="input-group">
			        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
			  <input id="email" placeholder="E-Mail Address" class="form-control"  type="text"/>
			    </div>
			  </div>
			  <div class="col-md-4"></div>
			</div>
			</div>
			<div class="row">
			
			<div class="form-group">
			  <label class="col-md-2 col-md-offset-2  control-label text-right">Message</label>
			    <div class="col-md-4 inputGroupContainer">
			    <div class="input-group">
			        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
			        	<textarea class="form-control" id="comment" placeholder="Enter Message Here"></textarea>
			  </div>
			  </div>
			  <div class="col-md-4"></div>
			</div>
			</div>
			<div class="row">
			
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-2 col-md-offset-2  control-label"></label>
			  <div class="col-md-4">
			    <button class="btn btn-warning" onclick="sendMessage();">Send <span class="glyphicon glyphicon-send"></span></button>
			  </div>
			</div>
			
			</div>
		</div>



<script>


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function sendMessage()
{
	var name = $("#first_name").val();
	var surname = $("#last_name").val();
	var mail = $("#email").val();
	var bd = $("#comment").val();

	var ers = {};
	
	if(name.trim() == "")
		ers["first_name"] = "First Name Field is empty";
	if(surname.trim() == "")
		ers["last_name"] = "Last Name Field is empty";
	if(!validateEmail(mail.trim()))
		ers["email"] = "Email is invalid";
	if(bd.trim() == "")
		ers["comment"] = "Message Body is empty";
	
	
	if(Object.size(ers) > 0)		
		displayAndCallGrowlForDanger(ers);	
	else
		callController("/sendContactMail",{'name': name, 'surname': surname, 'mail': mail, 'body': bd },'POST' , function()
				{
				
				 $("#first_name").val("");
				$("#last_name").val("");
				$("#email").val("");
				$("#comment").val("");
				});
}
</script>







</body>
</html>



