
<style type="text/css">
body	
{
background:url(/resources/imgs/Bmp_MM_background.png)  no-repeat center center fixed;
 -webkit-background-size: contain;
  -moz-background-size: contain;
  -o-background-size: contain;
  background-size: contain;
  background-color: black;
} 

#mainnews {
background: rgba(150, 2, 3, 0.91); /* Old browsers */
background: -moz-linear-gradient(left, rgba(150, 2, 3, 0.91) 0%, rgba(0,0,0,0) 30%, rgba(0,0,0,0) 30%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, rgba(150, 2, 3, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, rgba(150, 2, 3, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed8081', endColorstr='#000000',GradientType=1 ); /* IE6-9 */
}


#redernews {
background: rgba(2, 150, 3, 0.91); /* Old browsers */
background: -moz-linear-gradient(left, rgba(2, 150, 3, 0.91) 0%, rgba(0,0,0,0) 30%, rgba(0,0,0,0) 30%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, rgba(2, 150, 3, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, rgba(2, 150, 3, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed8081', endColorstr='#000000',GradientType=1 ); /* IE6-9 */
}

#brknews {
background: rgba(3, 2, 150, 0.91); /* Old browsers */
background: -moz-linear-gradient(left, rgba(3, 2, 150, 0.91) 0%, rgba(0,0,0,0) 30%, rgba(0,0,0,0) 30%); /* FF3.6-15 */
background: -webkit-linear-gradient(left, rgba(3, 2, 150, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to right, rgba(3, 2, 150, 0.91) 0%,rgba(0,0,0,0) 30%,rgba(0,0,0,0) 30%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed8081', endColorstr='#000000',GradientType=1 ); /* IE6-9 */
}



.jumbotron
{
	background-color:black;
}



</style>




<!-- NAVBAR -->

  <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" style='font-size: 17px;font-family: Consolas, monaco, monospace;' href="/">Tamorri Interactive Studios</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="/">Home</a></li>
            <li ><a href="#" onclick='goToPage("#navArea","/Reder_Engine",true);return false;'>Reder 3D Engine</a></li>
            <li ><a href="#" onclick='goToPage("#navArea","/Broken_Promises",true);return false;'>Broken Promises: The Game</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
           <li><a href="#" onclick='goToPage("#navArea","/Contacts",true);return false;'>Contact The Team</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
    
    
