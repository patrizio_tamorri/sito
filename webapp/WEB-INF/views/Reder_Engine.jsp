<%@page import="com.crystal.engine.Radius.spring.RadiusPageHelpers"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Reder Engine</title>
<link rel="icon" media="image/png" href="/resources/imgs/favicon.ico" />



<r_radius_imports
	general_theme_number="<%=(Integer)session.getAttribute("THEME")%>"
	datepicker_theme_number="<%=(Integer)session.getAttribute("THEME")%>"
	slider_theme_number="<%=(Integer)session.getAttribute("THEME")%>"
	forceSecurity />

<r_seo_specs
	description="Reder is an extremely fast Directx 12 3D Engine composed of several indipendent modules"
	keywords="Tamorri,Interactive,studios,Reder,engine,directx,12"
	language="en" robots="all" />
</head>
<body>

<script type="text/javascript">
$.unblockUI();
setPageBlockCallback(function(){});
$.unblockUI();
</script>


	<%@include file="support/Template.jsp"%>

	<div id="navArea"  style='min-height: 90%;'>

		<div class="jumbotron"
			>
			<div class="container">
				<h1 class="display-3">Reder Engine</h1>
				<div class="row">
					<div class="col-sm-4">
							<video  autoplay loop muted width="350" height="275"  style='border: 6px double rgba(132, 20, 20, 0.55);'>
								<source src="/resources/videos/Logo_Reder.mp4" type="video/mp4" />
							</video>
						
					</div>
					<div class="col-sm-7">
						<p style='padding-top: 64px; padding-left: 10px;'>Reder Engine is the second version of in-house PC engine.
							It implements a configurable module system, orchestrated in a
							multithreaded way. Modules actually provided contain a Directx 12
							multi-device implementation with Javascript configuration</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container"  style='padding-bottom: 30px'>
			<div class="row" id="news"></div>
		</div>


		<script>

$(function(){

	callController("getRederNews",{},"POST",function(datas){
		
		for(var i in datas)
			$("#news").append(" <div class=\"col-md-6 grad\"    ><h2 id='redernews'>"+datas[i]["TITLE"]+"</h2><p>"+ datas[i]["BODY"] +"</p>" +
					(datas[i]["LINK"] != null ? "<p><a class=\"btn btn-secondary\" href=\"#\" onclick='goToPage(\"#navArea\",\""+datas[i]["LINK"]+"\",true);return false;' role=\"button\">Find more &raquo;</a></p>" : "") +
		       "</div>");
		return true;		
	});
	
	
});

</script>


	</div>

	<footer style='bottom: 2px; top: auto; position:relative; width: 100%'>
		<hr />
		<p>&copy; All rights reserved</p>
	</footer>

</body>
</html>
