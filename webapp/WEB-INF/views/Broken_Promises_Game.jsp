<%@page import="com.crystal.engine.Radius.spring.RadiusPageHelpers"%>
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1"/>
 <title>Broken Promises</title>
<link rel="icon" media="image/png" href="/resources/imgs/favicon.ico"/>
<r_seo_specs 
description="Broken Promises is an in-development videogame by Tamorri Interactive Studios" 
keywords="Tamorri,Interactive,studios,directx,12,Broken,Promises" 
language="en"
robots="all"
/>


<r_radius_imports
	general_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	datepicker_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	slider_theme_number="<%=(Integer) session.getAttribute("THEME")%>"
	forceSecurity />
</head>
<body>

<script type="text/javascript">
$.unblockUI();
setPageBlockCallback(function(){});
$.unblockUI();
</script>


<%@include file="support/Template.jsp" %>

	
		<div id="navArea"  style='min-height: 90%;'>

		<div class="jumbotron"
			>
			<div class="container">
				<h1 class="display-3">Broken Promises</h1>
				<div class="row">
					<div class="col-sm-4">
							<video  autoplay loop muted width="350" height="275"  style='border: 6px double rgba(132, 20, 20, 0.55);'>
								<source src="/resources/videos/Broken Promises.mp4" type="video/mp4" />
							</video>
						
					</div>
					<div class="col-sm-7">
						<p style='padding-top: 64px; padding-left: 10px;'>Broken Promises is an RPG title 
						currently in development. Soon more informations.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container"  style='padding-bottom: 30px'>
			<div class="row" id="news"></div>
		</div>



<script>

$(function(){

	callController("getBrokenPromisesNews",{},"POST",function(datas){
		
		for(var i in datas)
			$("#news").append(" <div class=\"col-md-6 grad\"  ><h2 id='brknews'>"+datas[i]["TITLE"]+"</h2><p>"+ datas[i]["BODY"] +"</p>" +
					(datas[i]["LINK"] != null ? "<p><a class=\"btn btn-secondary\" href=\"#\" onclick='goToPage(\"#navArea\",\""+datas[i]["LINK"]+"\",true);return false;' role=\"button\">Find more &raquo;</a></p>" : "") +
		       "</div>");
		return true;		
	});
	
	
});

</script>


	</div>

	<footer style='bottom: 2px; top: auto; position:relative; width: 100%'>
		<hr />
		<p>&copy; All rights reserved</p>
	</footer>

</body>
</html>
